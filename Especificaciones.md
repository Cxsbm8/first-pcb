## Driver Motor Dc especificaciones de diseño:

### Funcionalidad esperada:
Con esta PCB se pretende controlar motores DC mediante una señal de control por infrarrojo.

### Condiciones eléctricas:
* Señal de entrada: Un haz infrarrojo entra a través de un receptor.
* Señal de salida: La PCB va a contar con bornes donde se podrá conectar el motor DC.
* Alimentación: Batería de 12 V.
* Ruido: La PCB se diseñará esperando que no esté expuesta a ruido electromagnético.

### Condiciones ambientales y mecánicas:
* Condiciones ambientales: se espera poder utilizar la tarjeta en diversas zonas con temperaturas de entre los 17 y 32 grados Celsius.
* Instalación mecánica:
-_ Pendiente_

### Protecciones:
Se planea que el motor lo pueda utilizar cualquier persona incluso si no tiene conocimientos sobre electrónica, por lo que se podría encapsular para que solo haya contacto directo con el control remoto. Asimismo, se incluye un fusible de protección ante sobre corrientes.
